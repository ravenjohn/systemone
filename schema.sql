drop database systemonev3;
create database systemonev3;
use systemonev3;

DROP TABLE IF EXISTS college;
CREATE TABLE college(
	id int(11) auto_increment primary key,
	code varchar(8) NOT NULL,
	name varchar(64) NOT NULL
);

DROP TABLE IF EXISTS unit;
CREATE TABLE unit(
	id int(11) auto_increment primary key,
	collegeId int(1) NOT NULL,
	code varchar(8) NOT NULL,
	name varchar(64) NOT NULL
);

DROP TABLE IF EXISTS degree_program;
CREATE TABLE degree_program(
	id int(11) auto_increment primary key,
	collegeId int(1) NOT NULL,
	code varchar(16) NOT NULL,
	name varchar(64) NOT NULL,
	type enum('undergraduate','graduate','non-degree','non-baccalaureate') NOT NULL
);

DROP TABLE IF EXISTS course;
CREATE TABLE course (
	id int(11) auto_increment primary key,
	unitId int(11) NULL,
	code varchar(20) NOT NULL,
	title varchar(80) NULL,
	units int(1) DEFAULT 0,
	semesterOffered char(3) NULL
);


DROP TABLE IF EXISTS section;
CREATE TABLE section (
	id int(11) AUTO_INCREMENT primary key,
	courseId int(11) NOT NULL,
	unitId int(11) NULL,
	name varchar(20) NULL,
	capacity int(4) DEFAULT 0,
	slotCount int(4) DEFAULT 0,
	waitlistCount int(4) DEFAULT 0,
	status enum('OK','FOR DISSOLUTION','DISSOLVED','CONDITIONAL') NOT NULL DEFAULT 'OK',
	showInstructor enum('NONE','LECTURER ONLY','LAB/RECIT ONLY','ALL') NOT NULL DEFAULT 'NONE',
	laboratoryInstructor varchar(54) NULL,
	lectureInstructor varchar(54) NULL,
	recitationInstructor varchar(54) NULL,
	daysLaboratory varchar(5) NULL,
	daysLecture varchar(5) NULL,
	daysRecitation varchar(5) NULL,
	timeLaboratory varchar(20) NULL,
	timeLecture varchar(20) NULL,
	timeRecitation varchar(20) NULL,
	roomLaboratory varchar(30) NULL,
	roomLecture varchar(30) NULL,
	roomRecitation varchar(30) NULL,
	daysLaboratory2 varchar(5) NULL,
	daysLecture2 varchar(20) NULL,
	daysRecitation2 varchar(5) NULL,
	timeLaboratory2 varchar(20) NULL,
	timeLecture2 varchar(20) NULL,
	timeRecitation2 varchar(20) NULL,
	roomLaboratory2 varchar(30) NULL,
	roomLecture2 varchar(30) NULL,
	roomRecitation2 varchar(30) NULL
);

DROP TABLE IF EXISTS slot;
CREATE TABLE slot(
	id int(11) AUTO_INCREMENT primary key,
	userId int(11) NULL,
	sectionId int(11) NULL,
	status enum('FREE','ENLISTED','RESERVED','FOR CLEARANCE','LOCKED') DEFAULT 'FREE'
);

DROP TABLE IF EXISTS __user;
CREATE TABLE __user(
	id int(11) primary key auto_increment,
	accessToken varchar(40) NULL,
	username varchar(32) NOT NULL,
	password varchar(32) NOT NULL,
	emailAddress varchar(64) NOT NULL,
	firstName varchar(64) NOT NULL,
	middleName varchar(64) NOT NULL,
	lastName varchar(64) NOT NULL,
	sex enum('Female','Male','Lesbian','Gay','Bisexual','Transexual','Other','Undefined') NOT NULL DEFAULT 'Undefined',
	active BOOLEAN NOT NULL DEFAULT FALSE,
	userType enum('student','employee') NOT NULL,

	employeeNumber varchar(11) NULL,
	unitId int(11) NULL,

	studentNumber varchar(11) NULL,
	curriculumCode varchar(62) NULL,
	allowedUnits tinyint(2) DEFAULT 0,
	degreeProgramId int(11) NULL,
	adjustedAllowedUnits BOOLEAN DEFAULT FALSE,

	ipAddress varchar(16) NULL,
	lastActivity timestamp NULL,
	dateCreated timestamp NULL,
	dateUpdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS __role;
CREATE TABLE __role(
	id int(11) primary key auto_increment,
	userId int(11) NOT NULL,
	role varchar(32) NOT NULL,
	status enum('Pending','Active','Deactivated','Banned','For Removal') NOT NULL DEFAULT 'Pending',
	dateCreated timestamp NULL DEFAULT NULL,
	dateUpdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);


DROP TABLE IF EXISTS recommended_course;
CREATE TABLE recommended_course (
	id int(11) primary key auto_increment,
	courseId int(11) NOT NULL,
	userId int(11) NOT NULL,
	priority enum('1','2') NOT NULL DEFAULT '1',
	p float DEFAULT NULL,
	dateCreated timestamp NULL,
	dateUpdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS __general_setting;
CREATE TABLE __general_setting(
	id int(11) auto_increment primary key,
	name varchar(256) NOT NULL,
	value varchar(256) NOT NULL
);


DROP TABLE IF EXISTS waitlisted;
CREATE TABLE waitlisted (
	id int(11) primary key auto_increment,
	userId varchar(10) NOT NULL,
	slotId int(11) NOT NULL,
	sectionId int(11) NOT NULL,
	dateCreated timestamp NULL,
	dateUpdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS finalized_student;
CREATE TABLE finalized_student(
	id int(11) auto_increment primary key,
	userId int(11) NOT NULL,
	isPrinted boolean DEFAULT FALSE,
	dateCreated timestamp NULL DEFAULT NULL,
	dateUpdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);


DROP TABLE IF EXISTS straggler;
CREATE TABLE straggler (
	id int(11) auto_increment primary key,
	userId int(11) NOT NULL,
	dateCreated timestamp NULL DEFAULT NULL,
	dateUpdated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
