<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OAuth extends REST_Controller
{
	public $methods = array(
		'login_post' => array(
			'params' => array('username','password'),
			'description' => 'Login'
		),
		'logout_post' => array(
			'scopes' => array(),	//empty array means any role
			'description' => 'Logout'
		)
	);
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function login_post()
	{
		$data							= $this->user_model->login($this->data, $this->input->ip_address());
		$degree							= $this->user_model->get_by_id($data['degreeProgramId'], 'name', TABLE_DEGREE_PROGRAM);
		$data['degree']					= $degree['name'];
		$data['ps_form']				= $this->user_model->get_ps_form($data['id']);
		$data['waitlist']				= $this->user_model->get_waitlist($data['id']);
		$data['recommended_courses']	= $this->user_model->get_recommended_courses($data['id'], TRUE);
		$data['message']				= 'Login successful';
	
		unset($data['degreeProgramId']);
		$this->response($data);
	}
	
	public function logout_post()
	{
		$this->user_model->update($this->_user['id'], array('accessToken' => NULL,'ipAddress' => NULL));
		$this->response(array('message' => 'Logout successful'));
	}
}

