<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OCM extends REST_Controller
{
	public $methods = array(
		'enlist_post' => array(
			'scopes' => array(ROLE_STUDENT),
			'params' => array('recommendationId','sectionId'),
			'description' => 'Enlist'
		),
	);
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('section_model');
		$this->load->library('section');
		$this->load->helper('conflict_helper');
	}
	
	public function enlist_post()
	{
		
		$this->user_model->must_be_recommended($this->_user['id'], $this->data['recommendationId']);
		self::_check_numeric($this->data['sectionId'], 'Section ID');
		$this->section_model->enlist($this->data['sectionId'], $this->_user['id']);
		$data['message'] = 'Successfully enlisted';
		$this->response($data);
	}
	
	public function logout_post()
	{
		$this->user_model->update($this->_user['id'], array('accessToken' => NULL,'ipAddress' => NULL));
		$this->response(array('message' => 'Logout successful'));
	}
}

