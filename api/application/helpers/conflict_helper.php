<?php


function check_conflict($s1, $s2)
{
	// e.g. s1 = 8-10,  s2 = 9-10
	for($i=0, $j=count($s1->scheds); $i < $j; $i += 1){	//loop for schedules of the 1st section
		for($k=0, $l=count($s2->scheds); $k < $l; $k += 1){	//loop for schedules of the 2nd section
		
			for($m=0, $n=strlen($s1->scheds[$i]['days']); $m < $n; $m += 1){	//loop for the days of the 1st section
			
				if(strpos( $s2->scheds[$k]['days'], $s1->scheds[$i]['days'][$m]) !== FALSE &&	// e.g. has both (M)ondays
					(
					($s1->scheds[$i]['time'][0] >= $s2->scheds[$k]['time'][0] && $s1->scheds[$i]['time'][0] < $s2->scheds[$k]['time'][1]) ||	// e.g. 8am between s2's 9-10?
					($s1->scheds[$i]['time'][1] > $s2->scheds[$k]['time'][0] && $s1->scheds[$i]['time'][1] <= $s2->scheds[$k]['time'][1]) ||	// e.g. 10am between s2's 9-10?
					($s1->scheds[$i]['time'][0] <= $s2->scheds[$k]['time'][0] && $s1->scheds[$i]['time'][1] >= $s2->scheds[$k]['time'][1])	// e.g. 8-11am, 9-10am
					)
				){
					throw new Exception($s1->scheds[$i]['toString'] . ' is in conflict with ' . $s2->scheds[$k]['toString']);
				}
			}
			
		}
	}
}
