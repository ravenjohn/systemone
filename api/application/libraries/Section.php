<?php

class Section
{

	private static function toTime($a, $f = FALSE)
	{
		$a = explode(':', $a);
		$b = intval($a[0]);
		$a[0] = $b + (($b > 7 || ($b == 7 && $f))? -7 : 5);
		return intval( strlen($a) > 1 ? implode('', $a) : $a[0]*100 )
	}
	
	private static function toRangeTime($string){
		if($string == 'TBA')
		{
			return $string;
		}
		
		$s = explode('-', $string);
		return array(self::toTime($s[0], TRUE), self::toTime($s[1]));
	}
	
	private function initMeta($a, $b, $c, $d, $e)
	{
		$d = str_replace('Mon','M', $d);
		$d = str_replace('/Tues?/','T', $d);
		$d = str_replace('Wed','W', $d);
		$d = str_replace('/Th(urs)?/','H', $d); //yes, it's Hursday not Thursday
		$d = str_replace('Fri','F', $d);
		$d = str_replace('Sat','S', $d);
		$d = str_replace('M-W','MTW', $d);
		$d = str_replace('M-H','MTWH', $d);
		$d = str_replace('M-F','MTWHF', $d);
		$d = str_replace('T-H','TWH', $d);
		$d = str_replace('T-F','TWHF', $d);
		$d = str_replace('W-F','WHF', $d);
		$d = str_replace('-','', $d);
		$sectionName = $this->sectionName;
		
		if(strpos($this->name, 'lecture') !== FALSE)
		{
			$sectionName = explode('-', $sectionName);
			$sectionName = $sectionName[0];
		}
		$toString = implode(' ', array($this->courseCode, $sectionName, $d OR 'TBA', $e OR 'TBA'));
		return array(
			'name' => $a,
			'room' => $c OR 'TBA',
			'raw_days' => $d OR 'TBA',
			'raw_time' => $e OR 'TBA',
			'instructor' => $b OR 'TBA',
			'time' => ($e && self::toRangeTime($e)) OR 'TBA',
			'type' => str_replace('/[0-9]/', '', $this->name),
			'days' => $d OR 'TBA',
			'toString' => $toString
		);
	}
	
	function __construct($o)
	{
		$this = $this;
			
		$this->scheds = array();
		$this->units = $o['units'];
		$this->id = $o['sectionId'];
		$this->courseCode = $o['courseCode'];
		$this->sectionName = $o['sectionName'];
		$this->offeringUnit = $o['offeringUnit'];
		$this->capacity = $o['numberOfStudents'];
		
		$o['dLec'] && $this->scheds[] = $this->initMeta('lecture', $o['lectureInstructor'], $o['roomLecture'], $o['daysLecture'], $o['timeLecture']);
		$o['dLec2'] && $this->scheds[] = $this->initMeta('lecture2', $o['lectureInstructor'], $o['roomLecture2'], $o['daysLecture2'], $o['timeLecture2']);
		$o['dRec'] && $this->scheds[] = $this->initMeta('recitation', $o['recitationInstructor'], $o['roomRecitation'], $o['daysRecitation'], $o['timeRecitation']);
		$o['dLab'] && $this->scheds[] = $this->initMeta('laboratory', $o['laboratoryInstructor'], $o['roomLaboratory'], $o['daysLaboratory'], $o['timeLaboratory']);
		$o['dRec2'] && $this->scheds[] = $this->initMeta('recitation2', $o['recitationInstructor'], $o['roomRecitation2'], $o['daysRecitation2'], $o['timeRecitation2']);
		$o['dLab2'] && $this->scheds[] = $this->initMeta('laboratory2', $o['laboratoryInstructor'], $o['roomLaboratory2'], $o['daysLaboratory2'], $o['timeLaboratory2']);
	}
};