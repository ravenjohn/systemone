<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_USER;
		$this->has_date = TRUE;
		$this->columns = array(
			'id',
			'accessToken',
			'username',
			'password',
			'emailAddress',
			'firstName',
			'middleName',
			'lastName',
			'sex',
			'active',
			'userType',
			'employeeNumber',
			'unitId',
			'studentNumber',
			'curriculumCode',
			'allowedUnits',
			'degreeProgramId',
			'adjustedAllowedUnits',
			'ipAddress',
			'lastActivity',
			'dateCreated',
			'dateUpdated'
		);
		$this->hidden_columns = array('password');
	}
	
	public function get_user_by_access_token($access_token, $ip_address)
	{
		$table = $this->table_name;
		$query = $this->db->select("id, username, userType, lastActivity")->from($table)->where(array('accessToken' => $access_token, 'ipAddress' => $ip_address))->get();
		if($query->num_rows() == 1)
		{
			$user			= $query->row_array();
			$lastActivity	= strtotime($user['lastActivity']);
			$timeNow		= strtotime($this->_time);
			
			if($lastActivity < $timeNow -  60 * 60 * 5)
			{
				throw new Exception('Sorry but you\'ve been inactive for 5 hours. Please login again.');
			}
			
			$roles = $this->db->select()->from(TABLE_ROLE)->where(array('userId' => $user['id']))->get();
			$roles = $roles->result_array();
			
			foreach($roles as $role)
			{
				$user['roles'][] = $role['role'];
			}
			
			$this->update($user['id'], array('lastActivity' => $this->_time));
			return $user;
		}
		return FALSE;
	}
	

	public function login($user, $ip_address)
	{
		$query = $this->db->get_where($this->table_name, array('username' => $user['username'], 'password' => md5($user['password'])));
		$user = $query->row_array();
		
		if(empty($user))
		{
			throw new Exception('The username or password you entered is incorrect');
		}
		
		$access_token = sha1(SALT . time() . SALT . uniqid() . SALT);
		$user = $this->update($user['id'], array('accessToken' => $access_token, 'ipAddress' => $ip_address, 'lastActivity' => $this->_time));
		unset($user['password']);
		return $user;
	}
	
	public function get_recommended_courses($user_id, $with_sections = FALSE)
	{
		// $result = get_from_cache("get_recommended_courses", $user_id);
		// if(!$result)
		// {
			$query = $this->db->query('select b.id as courseId, a.id as recommendationId, b.code, b.title from ' . TABLE_RECOMMENDED_COURSE . ' a, ' . TABLE_COURSE . ' b where a.courseId = b.id AND a.userId = ' . $user_id . ';');
			$result = $query->result_array();
			// put_in_cache('get_recommended_courses',$user_id,$result);
		// }
		if($with_sections)
		{
			foreach($result as &$datum)
			{			
				// $result2 = get_from_cache("get_recommended_courses_with_section", $datum['courseId']);
				// if(!$result2)
				// {
					$query = $this->db->query("select a.id, a.name, a.capacity as cap, a.slotCount as sC, a.waitlistCount as wC, a.status as stat, a.daysLaboratory as dLab, a.daysLecture as dLec, a.daysRecitation as dRec, a.timeLaboratory as tLab, a.timeLecture as tLec, a.timeRecitation as tRec, a.roomLaboratory as rLab, a.roomLecture as rLec, a.roomRecitation as rRec, a.daysLaboratory2 as dLab2, a.daysLecture2 as dLec2, a.daysRecitation2 as dRec2, a.timeLaboratory2 as tLab2, a.timeLecture2 as tLec2, a.timeRecitation2 as tRec2, a.roomLaboratory2 as rLab2, a.roomLecture2 as rLec2, a.roomRecitation2 as rRec2 from " . TABLE_SECTION . " a where a.courseId = ?;", array($datum['courseId']));
					$result2 = $query->result_array();
					// put_in_cache('get_recommended_courses_with_section',$datum['courseId'],$result2);
				// }
				$datum['sections'] = $result2;
			}
		}
		return $result;
	}
	
	public function get_ps_form($user_id)
	{
		// $result = get_from_cache("get_ps_form", $user_id);
		// if(!$result)
		// {
			$query = $this->db->query("select a.id as sectionId, b.id as slotId from ". TABLE_SLOT ." b, ". TABLE_SECTION ." a where b.userId = ? AND b.sectionId = a.id;", array($user_id));
			$result = $query->result_array();
			// put_in_cache('get_ps_form',$user_id,$result);
		// }
		return $result;
	}
	
	public function get_waitlist($user_id)
	{
		// $result = get_from_cache("get_waitlist", $user_id);
		// if(!$result)
		// {
			$query = $this->db->query("SELECT b.id, a.id as sectionId, CONCAT((SELECT(SELECT COUNT(*) + 1 FROM ".TABLE_WAITLISTED." WHERE id < X.id AND sectionId = a.id ) as rank FROM ".TABLE_WAITLISTED." as X WHERE userId = ? AND sectionId = b.sectionId),' of ' , (SELECT COUNT(*) FROM ".TABLE_WAITLISTED." WHERE sectionId = a.id)) as rank FROM ".TABLE_SECTION." a, ".TABLE_WAITLISTED." b WHERE b.sectionId = a.id AND b.userId = ?;", array($user_id, $user_id));
			$result = $query->result_array();
			// put_in_cache('get_waitlist',$user_id,$result);
		// }
		return $result;
	}
	
	public function must_be_recommended($user_id, $course_id)
	{
		
	}
}

