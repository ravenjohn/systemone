<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_ROLE;
		$this->has_date = TRUE;
		$this->columns = array(
			'id',
			'userId',
			'role',
			'status',
			'dateCreated',
			'dateUpdated'
		);
		
	}
	
	//model specific methods...
}

