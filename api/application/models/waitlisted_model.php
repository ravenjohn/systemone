<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Waitlisted_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_WAITLISTED;
		$this->has_date = TRUE;
		$this->columns = array(
			'id',
			'userId',
			'slotId',
			'sectionId',
			'dateCreated',
			'dateUpdated'
		);
	}
	
}

