<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();

		$this->table_name = TABLE_SECTION;
		
		$this->columns = array(
			'id',
			'courseId',
			'unitId',
			'name',
			'capacity',
			'slotCount',
			'waitlistCount',
			'status',
			'showInstructor',
			'laboratoryInstructor',
			'lectureInstructor',
			'recitationInstructor',
			'daysLaboratory',
			'daysLecture',
			'daysRecitation',
			'timeLaboratory',
			'timeLecture',
			'timeRecitation',
			'roomLaboratory',
			'roomLecture',
			'roomRecitation',
			'daysLaboratory2',
			'daysLecture2',
			'daysRecitation2',
			'timeLaboratory2',
			'timeLecture2',
			'timeRecitation2',
			'roomLaboratory2',
			'roomLecture2',
			'roomRecitation2'
		);
	}
	
	public function enlist($section_id, $user_id)
	{
		//check if conflict
		
		$res = $this->db->query('select * from slot a where a.sectionId = ? and a.userId = ? and a.status != "FREE"', array($section_id, $user_id))->num_rows;
		if($res === 0)
		{
			$this->db->query('update slot a set a.userId = ?, a.status = "RESERVED" where a.sectionId = ? and a.status = "FREE" limit 1', array($user_id, $section_id));
			$res = $this->db->query('select * from slot a where a.sectionId = ? and a.userId = ? and a.status = "RESERVED"', array($section_id, $user_id))->result_array();
			if(empty($res))
			{
				throw new Exception('No more slots');
			}
			return TRUE;
		}
		else
		{
			throw new Exception('Tama na ang isa.');
		}
	}
}

