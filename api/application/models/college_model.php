<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class College_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_COLLEGE;
		
		$this->columns = array(
			'id',
			'code',
			'name'
		);
	}
	
}

