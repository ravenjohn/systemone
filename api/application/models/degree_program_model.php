<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Degree_program_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_DEGREE_PROGRAM;
		
		$this->columns = array(
			'id',
			'collegeId',
			'code',
			'name',
			'type'
		);
	}
	
}

