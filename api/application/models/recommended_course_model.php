<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recommended_course_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = RECOMMENDED_COURSE;
		$this->has_date = TRUE;
		$this->columns = array(
			'id',
			'courseId',
			'userId'
			'priority',
			'p',
			'dateCreated',
			'dateaUpdated'
		);
	}
	
}

