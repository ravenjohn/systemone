<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slot_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_SLOT;
		
		$this->columns = array(
			'id',
			'userId',
			'sectionId',
			'status'
		);
	}
	
}

