<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_COURSE;
		
		$this->columns = array(
			'id',
			'unitId',
			'code',
			'title',
			'units',
			'semesterOffered'
		);
	}
	
}

