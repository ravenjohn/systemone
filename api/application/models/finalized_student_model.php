<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finalized_student_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_FINALIZED_STUDENT;
		$this->has_date = TRUE;
		$this->columns = array(
			'id',
			'userId',
			'isPrinted',
			'dateCreated',
			'dateUpdated'
		);
	}
	
}

