<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unit_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_UNIT;
		
		$this->columns = array(
			'id',
			'collegeId',
			'code',
			'name'
		);
	}
	
}

