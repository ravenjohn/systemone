<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends REST_Model
{

	function __construct()
	{
		parent::__construct();
		
		$this->table_name = TABLE_SETTING;
		
		$this->columns = array(
			'id',
			'name',
			'value'
		);
	}
	
}

