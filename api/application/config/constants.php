<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Query Constants
|--------------------------------------------------------------------------
|
| 
|
*/
define('DEFAULT_QUERY_LIMIT', 20);

/*
|--------------------------------------------------------------------------
| SALT
|--------------------------------------------------------------------------
|
| 
|
*/
define('SALT', 'fdbf4dacf02b349f4dfb7822231189d8b58c3576');

/*
|--------------------------------------------------------------------------
| Application Roles
|--------------------------------------------------------------------------
|
| To ease role renaming
|
*/
define('ROLE_OSA',				'OSA');
define('ROLE_STUDENT',			'Student');
define('ROLE_ENLISTOR',			'Enlistor');
define('ROLE_OBSERVER',			'Observer');
define('ROLE_REGISTRAR',		'Registrar');
define('ROLE_INSTRUCTOR',		'Instructor');
define('ROLE_ADMIN',			'Administrator');
define('ROLE_DEPARTMENT_HEAD',	'Department Head');
define('ROLE_COL_SEC',			'College Secretary');


/*
|--------------------------------------------------------------------------
| Database Tables
|--------------------------------------------------------------------------
|
| To ease table renaming
|
*/

define('TABLE_SLOT',				'slot');
define('TABLE_UNIT',				'unit');
define('TABLE_USER',				'__user');
define('TABLE_ROLE',				'__role');
define('TABLE_COURSE',				'course');
define('TABLE_COLLEGE',				'college');
define('TABLE_SECTION',				'section');
define('TABLE_STRAGGLER',			'straggler');
define('TABLE_WAITLISTED',			'waitlisted');
define('TABLE_ANNOUNCEMENT',		'announcement');
define('TABLE_DEGREE_PROGRAM',		'degree_program');
define('TABLE_SETTING',				'__general_setting');
define('TABLE_FINALIZED_STUDENT',	'finalized_student');
define('TABLE_RECOMMENDED_COURSE',	'recommended_course');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */